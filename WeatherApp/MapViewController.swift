//
//  MapViewController.swift
//  WeatherApp
//
//  Created by Venkata rao on 07/01/21.
//

import UIKit
import MapKit
import CoreData
class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var currentLocation = LocationManager()
    lazy var networkRest = RestManager()
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        let add = UIBarButtonItem(title:"Add Location", style: .plain, target: self, action: #selector(addLocation))
        add.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
       
        self.navigationItem.rightBarButtonItems = [add]
        currentLocation.activateLocation()
        currentLocation.delegate = self
        // Do any additional setup after loading the view.
    }
  
    @objc func addLocation(){
        self.showSpinner(onView: self.view)
        self.getAddressFromLatLon(pdblLatitude: String(self.mapView.centerCoordinate.latitude), withLongitude: String(self.mapView.centerCoordinate.longitude))
        
       
      
        
        
    }
    func getWeatherReport(city:String) {
        guard let url  = URL(string:"http://api.openweathermap.org/data/2.5/weather?lat=\(self.mapView.centerCoordinate.latitude)&lon=\(self.mapView.centerCoordinate.longitude)&appid=fae7190d7e6433ec3a45285ffcf55c86&units=metric") else { return  }
        
        self.networkRest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            self.removeSpinner()
            if let data = results.data {
                do {
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(CurrentForecastModel.self, from: data)
                    CoreDataManager.shared.addLocation(model: responseModel,city:city)
                    DispatchQueue.main.async {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                } catch  {
                    
                }
                
            }
            
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            let lat: Double = Double("\(pdblLatitude)")!
            //21.228124
            let lon: Double = Double("\(pdblLongitude)")!
            //72.833770
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]

                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country ?? "")
                        print(pm.locality ?? "")
                        print(pm.subLocality ?? "")
                        print(pm.thoroughfare ?? "")
                        print(pm.postalCode ?? "")
                        print(pm.subThoroughfare ?? "")
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            self.getWeatherReport(city: pm.subLocality ?? "")
                            return
                                }
                      
                        if pm.locality != nil {
                            self.getWeatherReport(city: pm.locality ?? "")
                            return
                            
                        }
                        self.getWeatherReport(city: pm.country ?? "")
                      
                  }
            })

        }

    
}



extension MapViewController:MKMapViewDelegate{
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        
    }
}


extension MapViewController: LocationManagerDelegate {
    
    func getCurrentLocation(lattitude:Double?,longitude:Double?) {

        currentLocation.stopLocation()
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: CLLocationDegrees(lattitude ?? 0.0), longitude: CLLocationDegrees(longitude ?? 0.00)), latitudinalMeters: CLLocationDistance(exactly: 500)!, longitudinalMeters: CLLocationDistance(exactly: 500)!)
        self.mapView.setRegion(mapView.regionThatFits(region), animated: true)
        
    }
    
    func locationDisabled(){
       
    }
    
}
