//
//  CoreDataManager.swift
//  WeatherApp
//
//  Created by Venkata rao on 07/01/21.
//

import Foundation
import CoreData
import UIKit
class CoreDataManager {
    static let tableName = "LocationModel"
    static let shared = CoreDataManager()
    
    
    func addLocation(model:CurrentForecastModel? , city:String) {
        DispatchQueue.main.async { 
            
            if let dataObj = model {
                let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
                let entityDescripition = NSEntityDescription.entity(forEntityName: CoreDataManager.tableName, in: managedObjectContext)
                
                let loc = LocationModel(entity: entityDescripition!, insertInto: managedObjectContext)
                loc.latitude = String(dataObj.coord?.lat ?? 0)
                loc.longitude = String(dataObj.coord?.lon ?? 0)
                if city.count > 0 {
                    loc.locationName = city
                }else{
                    loc.locationName = String(dataObj.name ?? "-")
                }
                
                loc.locationId = String(dataObj.id ?? 0)
                loc.createdDate = Date()
                if let weather = dataObj.weather,weather.count > 0{
                    loc.status = weather[0].main ?? "--"
                    loc.icon  =  weather[0].icon ?? ""
                }
                if let temp = dataObj.main{
                    loc.temparature = String(Int(temp.temp ?? 0))
                }
                
                //  loc.status = String(data.main?.)
                do {
                    try managedObjectContext.save()
                } catch _ {
                }
                
            }
        }
        
    }
    
    func loadLocations() -> [LocationModel] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return []
        }
        
        let context = appDelegate.managedObjectContext
        
        
        let request: NSFetchRequest<LocationModel> = LocationModel.fetchRequest()
        
        let sort = NSSortDescriptor(key:"createdDate", ascending: false)
        request.sortDescriptors = [sort]
           // request.predicate = NSPredicate(format: "title = %@", "MEDIUM")
        do {
            
            let articles = try context.fetch(request)
            
            
            return articles
        }  catch {
            fatalError("This was not supposed to happen")
        }
        
        
    }
    
    func delete(model:LocationModel?) {
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        managedObjectContext.delete(model ?? LocationModel())
        do {
            try managedObjectContext.save()
        }  catch {
            fatalError("This was not supposed to happen")
        }
    }
    
    
}


extension LocationModel {
    
    @nonobjc class func fetchRequest() -> NSFetchRequest<LocationModel> {
        return NSFetchRequest<LocationModel>(entityName: "LocationModel")
    }
}
