//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Venkata rao on 07/01/21.
//

import Foundation
import MapKit

protocol LocationManagerDelegate {
    func getCurrentLocation(lattitude:Double?,longitude:Double?)
    func locationDisabled()
}

class LocationManager:NSObject {
        
    var locationManager = CLLocationManager()
    var delegate: LocationManagerDelegate?
    var currentLocation: CLLocationCoordinate2D?
    
    override init() {
        locationManager = CLLocationManager()
    }
    
    func stopLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    func activateLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
           switch CLLocationManager.authorizationStatus() {
            //.notDetermined,
           case .notDetermined:break
                case  .restricted, .denied:
                    print("No access")
                    self.delegate?.locationDisabled()
                    break;
                case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                    break;
          
           @unknown default:
                 break
            }
                
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
}

extension LocationManager :CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.delegate?.getCurrentLocation(lattitude:locValue.latitude , longitude: locValue.longitude)
        currentLocation = locValue
    }
}
