//
//  DesignUtils.swift
//  WeatherApp
//
//  Created by Venkata rao on 07/01/21.
//

import Foundation
import UIKit
@IBDesignable
open class HEXAVIew: UIView {
public override init(frame: CGRect) {
        super.init(frame: frame)
       // setTitleColor(UIColor.blue, for: .normal)
    }
public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
           didSet {
                 self.layer.cornerRadius = self.cornerRadius
           }
    }
}
