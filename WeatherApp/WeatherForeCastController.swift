//
//  WeatherForeCastController.swift
//  WeatherApp
//
//  Created by Venkata rao on 07/01/21.
//

import UIKit

class WeatherForeCastController: UIViewController {
    lazy var networkRest = RestManager()
    var location:LocationModel?
    var forcastDictionary:[String:[CurrentForecastModel]] = [String:[CurrentForecastModel]]()
    
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var forcastCollectionView: UICollectionView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var statusLbl: UILabel!
    
    @IBOutlet weak var forcastTableView: UITableView!
    
    @IBOutlet weak var temparatureLBl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        forcastTableView.tableFooterView = UIView(frame: .zero)
        // Do any additional setup after loading the view.
        self.getForcastData()
        //self.forcastTableView.register(LocationCell.self, forCellReuseIdentifier: "locationCell")
        self.forcastCollectionView.dataSource = self
        self.forcastCollectionView.delegate = self
        cityLbl.text = location?.locationName ?? ""
        statusLbl.text = location?.status ?? ""
        temparatureLBl.text = (location?.temparature ?? "") + "°"
         
    }
    
    func getForcastData(){
        self.showSpinner(onView: self.view)
        guard let url  = URL(string:"http://api.openweathermap.org/data/2.5/forecast?lat=\(self.location?.latitude ?? "")&lon=\(self.location?.longitude ?? "")&appid=fae7190d7e6433ec3a45285ffcf55c86&units=metric") else { return  }
        
        self.networkRest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            self.removeSpinner()
            if let data = results.data {
                do {
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(FiveDayForcastMOdel.self, from: data)
                   
                        self.formatDates(model: responseModel.list)
                    DispatchQueue.main.async {
                        
                    }
                   
                   
                  print(responseModel)
                } catch  {
                    
                }
                
            }
            
        }
        
        
    }
    
    func formatDates(model:[CurrentForecastModel]?){
        if let list = model {
            for forcast in list{
                if let dt = self.getDate(dateStr: forcast.dt_txt ?? "") {
                    if forcastDictionary.keys.contains(dt) {
                        var objects = forcastDictionary[dt]
                        objects?.append(forcast)
                        forcastDictionary[dt] = objects
                    }else{
                        forcastDictionary[dt] = [forcast]
                    }
                }
                
            }
        }
        //
        DispatchQueue.main.async {
            if let objects = self.forcastDictionary[self.getTodayDate()],objects.count > 0  {
                let dataObj = objects[0]
                if let weather = dataObj.weather,weather.count > 0{
                    self.statusLbl.text = weather[0].main ?? "--"
                    
                }
            }
            self.forcastCollectionView.reloadData()
            self.forcastTableView.reloadData()
        }
       
        
    }

    func getDate(dateStr:String) -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let date = dateFormatter.date(from: dateStr)

        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: date ?? Date())
    }
    func getTime(dateStr:String) -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let date = dateFormatter.date(from: dateStr)

        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "h a"
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: date ?? Date())
    }
    func getWeekName(dateStr:String) -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let date = dateFormatter.date(from: dateStr)

        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "EEEE"
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: date ?? Date())
    }
    
    func getTodayDate() ->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: Date())
        
    }
   
    func getAllDays()->[String]{
        var keys = forcastDictionary.keys.sorted()
        keys.remove(element:getTodayDate() )
        return keys
       
    }
}


extension WeatherForeCastController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dayCell", for: indexPath) as! DayCell
        
        let day = getAllDays()[indexPath.row]
        let object = forcastDictionary[day]?.first
        
        cell.weekNameLbl.text = getWeekName(dateStr: object?.dt_txt ?? "")
        if let weather = object?.weather,weather.count > 0{
            let icon  =  weather[0].icon ?? ""
            let url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
            cell.iconImage .loadImageUsingCacheWithURLString(url, placeHolder: UIImage(named: "placeholder"))
        }
        
        if let temp = object?.main{
            let te =  String(Int(temp.temp_min ?? 0))
            cell.temparature.text = te + "°"
            let max =  String(Int(temp.temp_max ?? 0))
            cell.humidity.text = max + "°"
        }
               
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  getAllDays().count
        }
   
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 260
        
    }
        
}

extension WeatherForeCastController:UICollectionViewDelegate,UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let objects = forcastDictionary[self.getTodayDate()] else {
            return 0
        }
        return objects.count
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
        let cell:TodayForcastCell = collectionView.dequeueReusableCell(withReuseIdentifier:"TodayForcastCell", for: indexPath as IndexPath) as! TodayForcastCell
       
        if let objects = forcastDictionary[self.getTodayDate()]  {
            let dataObj = objects[indexPath.row]
            if let weather = dataObj.weather,weather.count > 0{
              //  cell.time.text = weather[0].main ?? "--"
                let icon  =  weather[0].icon ?? ""
                let url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
                  cell.iconImage .loadImageUsingCacheWithURLString(url, placeHolder: UIImage(named: "placeholder"))
            }
            if let temp = dataObj.main{
                let te =  String(Int(temp.temp ?? 0))
                cell.temparature.text = te + "°"
            }
            if let time = self.getTime(dateStr: dataObj.dt_txt ?? ""){
               
                cell.time.text = time
            }
        }
      
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
     
    }

}
