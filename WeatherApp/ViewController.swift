//
//  ViewController.swift
//  WeatherApp
//
//  Created by Venkata rao on 06/01/21.
//

import UIKit
import Foundation

class ViewController: UIViewController {

    @IBOutlet weak var locationsTableView: UITableView!
    var locations:[LocationModel]?
    var array = ["alpha", "beta", "gamma"]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.currentReachabilityStatus)
        locationsTableView.tableFooterView = UIView(frame: .zero)
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        add.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.navigationItem.rightBarButtonItems = [add]
                
    }
    override func viewWillAppear(_ animated: Bool) {
        locations =  CoreDataManager.shared.loadLocations()
        locationsTableView.reloadData()
    }

    @objc func addTapped(){
        let mapView = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(mapView, animated: true)
    }

}

extension ViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! LocationCell
        
        cell.headingLbl.text = locations?[indexPath.row].locationName ?? ""
        cell.subHeadingLbl.text = locations?[indexPath.row].status ?? ""
        cell.weatherReportLbl.text = (locations?[indexPath.row].temparature ?? "") + "°"
          let url = "http://openweathermap.org/img/wn/" + (locations?[indexPath.row].icon ?? "") + "@2x.png"
            cell.weatherImage .loadImageUsingCacheWithURLString(url, placeHolder: UIImage(named: "placeholder"))
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations?.count ?? 0
        }
   
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath){
        let forcast = self.storyboard?.instantiateViewController(withIdentifier: "WeatherForeCastController") as! WeatherForeCastController
        forcast.location = self.locations?[indexPath.row]
        
        self.navigationController?.pushViewController(forcast, animated: true)
          }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
           return .delete
       }
  
    
    internal func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
           if editingStyle == .delete{
            
                let alert = UIAlertController(title: "Delete Note", message: "Are you sure you want to delete this note?", preferredStyle: .actionSheet)

                alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: {
                    (alert:UIAlertAction!) in
                    let obj = self.locations?[indexPath.row]
                   // self.locations.remove(at: indexPath.row)
                    CoreDataManager.shared.delete(model:obj)
                    
                    self.locations?.remove(element: obj ?? LocationModel())
                   tableView.beginUpdates()
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                    tableView.endUpdates()
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

                self.present(alert, animated: true, completion: nil)
            
              
               
           }
       }
    
    
        
}
