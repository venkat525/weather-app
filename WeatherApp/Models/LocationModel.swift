//
//  LocationModel.swift
//  WeatherApp
//
//  Created by Venkata rao on 07/01/21.
//

import Foundation
import CoreData

class LocationModel: NSManagedObject {

    @NSManaged var latitude: String?
    @NSManaged var locationId: String?
    @NSManaged var locationName: String?
    @NSManaged var longitude: String?
    @NSManaged var createdDate:Date?
    @NSManaged var status:String?
    @NSManaged var temparature:String?
    @NSManaged var icon:String?
}
