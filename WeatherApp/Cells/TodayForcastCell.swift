//
//  TodayForcastCell.swift
//  WeatherApp
//
//  Created by Venkata rao on 07/01/21.
//

import UIKit

class TodayForcastCell: UICollectionViewCell {
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var temparature: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
}
