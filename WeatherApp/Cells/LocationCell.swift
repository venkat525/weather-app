//
//  LocationCell.swift
//  WeatherApp
//
//  Created by Venkata rao on 07/01/21.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var headingLbl: UILabel!
    
    @IBOutlet weak var subHeadingLbl: UILabel!
    
    @IBOutlet weak var weatherReportLbl: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    

}
