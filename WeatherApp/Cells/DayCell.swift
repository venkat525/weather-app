//
//  DayCell.swift
//  WeatherApp
//
//  Created by Venkata rao on 08/01/21.
//

import UIKit

class DayCell: UITableViewCell {

    @IBOutlet weak var weekNameLbl: UILabel!
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var temparature: UILabel!
    @IBOutlet weak var humidity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
